Im = imread('diente.jpg');
[rows, columns, numberOfColorChannels] = size(Im);
if numberOfColorChannels > 1
   I = rgb2gray(Im); 
else
    I = Im;
end
[height, width, dim] = size(I);
%Bottom-hat filtered
radio = round(width/4);
se = strel('disk',radio);
%B = imbothat(I,se);
IM = imsubtract(imadd(I,imtophat(I,se)),imbothat(I,se)); %Imagen Mejorada

[f,co] = size(I);

%Para dibujar el grafico de las intensidades en el sentido horizontal------
ejeX = int32(zeros(1,f));
ejeY = linspace(1,f,f);
suma = 0;

for i = 1:f
    for j = 1:co
        valor = IM(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeX(i) = suma;
    suma = 0;
end

[maxX, maxIndX] = max(ejeX(:));
[minX, minIndX] = min(ejeX(:));

if minIndX > maxIndX
    [maxX2, maxIndX2] = max(ejeX(minIndX:f));
    maxIndX2 = minIndX + maxIndX2 - 1;
    %IC = imcrop(IM,[1 maxIndX co maxIndX2]);
else
    [maxX2, maxIndX2] = max(ejeX(1:minIndX));
    %IC = imcrop(IM,[1 maxIndX2 co maxIndX]);
end

%plot(ejeX,ejeY);
%--------------------------------------------------------------------------

%Encontrar los puntos de corte horizontal----------------------------------
arregloTemporal = int32(zeros(1,abs(maxIndX - maxIndX2) + 1));
puntosHorizontal = [];
absoluto = abs(maxIndX2-maxIndX) + 1;
for a = 1:co
    if maxIndX > maxIndX2
        contador = 1;
        for b = maxIndX2:maxIndX
            temporal = int32(IM(b,a));
            arregloTemporal(contador) = temporal;
            contador = contador + 1;
        end
        [minimo, indice] = min(arregloTemporal(:));
        arregloTemporal2 = [];
        [filas, columnas] = size(arregloTemporal);
        for p = 1:columnas
            if arregloTemporal(p) == minimo
                arregloTemporal2 = [arregloTemporal2 (p + maxIndX2 - 1)];
            end
        end
        promedio = mean(arregloTemporal2(:));
        promedio = round(promedio);
        puntosHorizontal = [puntosHorizontal promedio];
    else
        contador = 1;
        for c = maxIndX:maxIndX2
            temporal = int32(IM(c,a));
            arregloTemporal(contador) = temporal;
            contador = contador + 1;
        end
        [minimo, indice] = min(arregloTemporal(:));
        arregloTemporal2 = [];
        [filas, columnas] = size(arregloTemporal);
        for p = 1:columnas
            if arregloTemporal(p) == minimo
                arregloTemporal2 = [arregloTemporal2 (p + maxIndX - 1)];
            end
        end
        promedio = mean(arregloTemporal2(:));
        promedio = round(promedio);
        puntosHorizontal = [puntosHorizontal promedio];
    end
end
%--------------------------------------------------------------------------

%Dibujar el corte horizontal-----------------------------------------------
[fil, col] = size(puntosHorizontal);
resultado = Im;
for u = 1: col - 1
    resultado = func_DrawLine(resultado, puntosHorizontal(u), u, puntosHorizontal(u + 1), (u + 1), 255);
end
%--------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%PARA EL RECORTE VERTICAL%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IV = IM;
for j = 1: co
    for i = puntosHorizontal(j):f
        IV(i,j) = 0;
    end
end

%Para dibujar el grafico de las intensidades en el sentido vertical------
ejeX = linspace(1,co,co);
ejeY = int32(zeros(1,co));
suma = 0;
for j = 1:co
    for i = 1:f
        valor = IV(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeY(j) = suma;
    suma = 0;
end

%TF = islocalmin(ejeY);
%plot(ejeX,ejeY,ejeX(TF),ejeY(TF),'r*')

level = graythresh(IV);
IVB = imbinarize(IV,level);
IVB = bwareaopen(IVB, 50);

ejeX = linspace(1,co,co);
ejeY = int32(zeros(1,co));
suma = 0;
for j = 1:co
    for i = 1:f
        valor = IVB(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeY(j) = suma;
    suma = 0;
end

y = double(ejeY);
n = 5;
[pks,locs,~,prm] = findpeaks(-y);
[~,i] = sort(prm,'descend');
menoresEjeY = y(locs(i(1:n)));
menoresEjeX = locs(i(1:n));
UnionXY = [reshape(menoresEjeX,5,1) reshape(menoresEjeY,5,1)];
UnionXY = sortrows(UnionXY,1);
%figure; plot(1:numel(y),y,'-',locs(i(1:n)),y(locs(i(1:n))),'o ')

P = vertcat(ejeX(1,1), UnionXY(:,1), ejeX(1, co));
Q = vertcat(ejeY(1,1), UnionXY(:,2), ejeY(1, end));
TF2 = islocalmax(Q);
TF3 = islocalmin(Q);
[tamP, ~] = size(P);
minimosTF3 = [];
for i = 1:tamP
    if TF3(i) == 1
        minimosTF3 = [minimosTF3 Q(i)];
    end
end

%figure; plot(P,Q,P(TF2),Q(TF2),'r*')

%Eliminar picos altos
TF2 = islocalmax(Q);
vof = 1;
while any(TF2 == 1) && vof == 1
    vof = 0;
    [tamP, ~] = size(P);
    i = 1;
    while i <= tamP
        pertenece = ismember(Q(i),minimosTF3);
        if TF2(i) == 1 && pertenece == 0
            TF2(i) = [];
            P(i) = [];
            Q(i) = [];
            tamP = tamP - 1;
            vof = 1;
        else
            i = i + 1;
        end
    end
      TF2 = islocalmax(Q);
end
%figure; plot(P,Q)

%Analizar cada linea
[tamP, ~] = size(P);
inicioX = 1;
ejeXporLinea = [];
ejeYporLinea = [];
lineasInvalidas = [];
for i = 1:tamP
    if i ~= 1 && i ~= tamP
        ejeXporLinea = linspace(inicioX,P(i),(P(i)-inicioX) + 1);
        for ix = inicioX:P(i)
            for iy = 1:f
                valor = IV(iy,ix);       
                suma = int32(suma) + int32(valor);
            end
            ejeYporLinea = [ejeYporLinea suma];
            suma = 0;
        end
        [maxValor, maxIndice] = max(ejeYporLinea(:));
        if maxValor <= ejeYporLinea(1) || maxValor <= ejeYporLinea(end)
            lineasInvalidas = [lineasInvalidas P(i)];
        end
        inicioX = P(i);
        ejeYporLinea = [];
    end
end

[tam, ~] = size(lineasInvalidas);
i = 1;
j = 1;
while j <= tam
    if P(i) == lineasInvalidas(j)
        TF2(i) = [];
        P(i) = [];
        Q(i) = [];
        j = j + 1;
    else
        i = i + 1;
    end
end

[tamP, ~] = size(P);
resultado2 = IV;
v = 1;
for w = 1:co
    if v <= tamP
        if P(v) == w
            if w ~= 1 && w ~= co
                resultado2 = func_DrawLine(resultado2, 1, w, f, w, 255);               
            end
            v = v + 1;
        end
    else
        break;
    end
end

%figure; imshow(resultado2);
[tamP, ~] = size(P);
inicioX = 1;
ancho = round((co/(tamP - 1))/8);
PuntosVerticales = [];
for i = 1:tamP
    if i ~= 1 && i ~= tamP
        lineaVertical = [];
        for iy = 1:f
            menor = 255;
            ix2 = 1;
            if P(i) - ancho < 1
                ancho = P(i);
            elseif P(i) + ancho > co
                ancho = co - P(i);
            end
            for ix = P(i) - ancho:P(i) + ancho
                valor = I(iy,ix);
                if valor < menor
                    menor = valor;
                    ix2 = ix;
                end
            end
            lineaVertical = [lineaVertical ix2];
            %I(iy,ix2) = 255;
        end
        PuntosVerticales = vertcat(PuntosVerticales, lineaVertical);
        lineaVertical = [];
    end
end

resultadoVertical = resultado;
for i = 1:tamP - 2
    for j = 1:f - 1
        resultadoVertical = func_DrawLine(resultadoVertical, j, PuntosVerticales(i,j), (j + 1), PuntosVerticales(i, j+ 1), 255);
    end
end

imshow(resultadoVertical);
%for i=180:-1:1

%Mostrar la imagen final
%y = double(ejeY);

%[peak_value, peak_location] = findpeaks(-y);
%plot(ejeX,y,ejeX(peak_location),peak_value,'r*')

% [pks,locs,~,prm] = findpeaks(-y);
% [~,i] = sort(prm,'descend');
% plot(1:numel(y),y,'-',locs(i(1:5)),y(locs(i(1:5))),'o ')

%imshow(resultado2)
%--------------------------------------------------------------------------

%Funcion para graficar lineas en una imagen--------------------------------
function Img = func_DrawLine(Img, X0, Y0, X1, Y1, nG)
Img(X0, Y0) = nG;
Img(X1, Y1) = nG;
if abs(X1 - X0) <= abs(Y1 - Y0)
   if Y1 < Y0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1-Y0; dx = X1-X0;
      p = 2*dx; n = 2*dy - 2*dx; tn = dy;
      while (Y0 < Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 + 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dx; n = 2*dy + 2*dx; tn = dy;
      while (Y0 <= Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 - 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   end
else if X1 < X0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1 - Y0; dx = X1 - X0;
      p = 2*dy; n = 2*dx-2*dy; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 + 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dy; n = 2*dy + 2*dx; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 - 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   end
end
end
%--------------------------------------------------------------------------
 
% Rotar la imagen -8�
% g = -8;
% IR = imrotate(IM,g);
%--------------------------------------------------------------------------

%Mostrar el histograma de una imagen---------------------------------------
% histogram(I)
%--------------------------------------------------------------------------

%Fusionar dos imagenes-----------------------------------------------------
% C = imfuse(Im,Ifill,'blend','Scaling','joint');
%--------------------------------------------------------------------------

