Imagen = 'diente.jpg';
I = imread(Imagen);%obtener imagen
IF = imfilter(I, fspecial('average'));
EG = rgb2gray(IF); %escala de grises

% P = edge(EG, 'Prewitt');
S = edge(EG, 'sobel', 'Sobel');
% R = edge(EG, 'Roberts');
% L = edge(EG, 'log');
% Z = edge(EG, 'zerocross');
% C = edge(EG, 'Canny');
%imwrite(I, Imagen)%escribir imagen
%{
bw = im2bw(I, graythresh(I)); 
cc = bwconncomp(bw);
lblMatrix = labelmatrix(cc);

edgeMatrix=edge(lblMatrix,'log',0);
%}

% cc = bwconncomp(I);
% lblImg = labelmatrix(cc);
% imshow(lblImg);
% [gx,gy] = gradient(double(EG));
% EG((gx.^2+gy.^2)==0) = 0;
me = imfuse(I, S, 'blend', 'Scaling', 'join');
imshow(me);
% subplot(2,2,1), imshow(I), title('Original');
% subplot(2,2,2), imshow(IF), title('Filtro');
% subplot(2,2,3), imshow(EG), title('Gris');
% subplot(2,2,4), imshow(label2rgb(lblImg)), title('Borde');
