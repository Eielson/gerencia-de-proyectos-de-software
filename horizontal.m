Im = imread('diente.jpg');
I = rgb2gray(Im);
[height, width, dim] = size(I);
%Bottom-hat filtered
radio = round(width/4);
se = strel('disk',radio);
%B = imbothat(I,se);
IM = imsubtract(imadd(I,imtophat(I,se)),imbothat(I,se)); %Imagen Mejorada

[f,co] = size(I);

%Para dibujar el grafico de las intensidades en el sentido horizontal------
ejeX = int32(zeros(1,f));
ejeY = linspace(1,f,f);
suma = 0;

for i = 1:f
    for j = 1:co
        valor = IM(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeX(i) = suma;
    suma = 0;
end

[maxX, maxIndX] = max(ejeX(:));
[minX, minIndX] = min(ejeX(:));

if minIndX > maxIndX
    [maxX2, maxIndX2] = max(ejeX(minIndX:f));
    maxIndX2 = minIndX + maxIndX2 - 1;
else
    [maxX2, maxIndX2] = max(ejeX(1:minIndX));
end
%plot(ejeX,ejeY);
%--------------------------------------------------------------------------

%Encontrar los puntos de corte horizontal----------------------------------
arregloTemporal = int32(zeros(1,abs(maxIndX - maxIndX2) + 1));
puntosHorizontal = [];
absoluto = abs(maxIndX2-maxIndX) + 1;
for a = 1:co
    if maxIndX > maxIndX2
        contador = 1;
        for b = maxIndX2:maxIndX
            temporal = int32(IM(b,a));
            arregloTemporal(contador) = temporal;
            contador = contador + 1;
        end
        [minimo, indice] = min(arregloTemporal(:));
        arregloTemporal2 = [];
        [filas, columnas] = size(arregloTemporal);
        for p = 1:columnas
            if arregloTemporal(p) == minimo
                arregloTemporal2 = [arregloTemporal2 (p + maxIndX2 - 1)];
            end
        end
        promedio = mean(arregloTemporal2(:));
        promedio = round(promedio);
        puntosHorizontal = [puntosHorizontal promedio];
    else
        contador = 1;
        for c = maxIndX:maxIndX2
            temporal = int32(IM(c,a));
            arregloTemporal(contador) = temporal;
            contador = contador + 1;
        end
        [minimo, indice] = min(arregloTemporal(:));
        arregloTemporal2 = [];
        [filas, columnas] = size(arregloTemporal);
        for p = 1:columnas
            if arregloTemporal(p) == minimo
                arregloTemporal2 = [arregloTemporal2 (p + maxIndX - 1)];
            end
        end
        promedio = mean(arregloTemporal2(:));
        promedio = round(promedio);
        puntosHorizontal = [puntosHorizontal promedio];
    end
end
%--------------------------------------------------------------------------

%Dibujar el corte horizontal-----------------------------------------------
[fil, col] = size(puntosHorizontal);
resultado = Im;
for u = 1: col - 1
    resultado = func_DrawLine(resultado, puntosHorizontal(u), u, puntosHorizontal(u + 1), (u + 1), 255);
end
%--------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%PARA EL RECORTE VERTICAL%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IV = IM;
for j = 1: co
    for i = puntosHorizontal(j):f
        IV(i,j) = 255;
    end
end

%Para dibujar el grafico de las intensidades en el sentido vertical------
ejeX = linspace(1,co,co);
ejeY = int32(zeros(1,co));
suma = 0;
i = 1;
for j = 1:co
    for i = 1:f
        valor = IV(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeY(j) = suma;
    suma = 0;
end

TF = islocalmin(ejeY);
%plot(ejeX,ejeY,ejeX(TF),ejeY(TF),'r*')
resultado2 = IV;
for v = 1:co
    if TF(v) == 1
        resultado2 = func_DrawLine(resultado2, 1, v, f, v, 255);
    end
end

[~, tam] = size(TF);
puntosNegros = [];

% for j = 1: tam
%     if TF(j) == 1
%         menor = 255;
%         tempI = 1;
%         for i = 1:f
%             if IV(i,j) < menor
%                 menor = IV(i,j);
%                 tempI = i;
%             end
%         end
%         IV(tempI,j) = 255;
%         
%     end
% end
%Mostrar la imagen final
%imshow(resultado)
%--------------------------------------------------------------------------

%Formulas para dibujar las dibujar lineas horizontales
% pDi = c*(1- Di/(MAXk*Dk))
% pYi = 1/(raiz(2?)*?)*e(elevado(-((yi-y)^2)/(?^2)))
% pD = int32(zeros(1,f));
% pY = int32(zeros(1,f));
% pDi = 0;
% pYi = 0;

%Funcion para graficar lineas en una imagen--------------------------------
function Img = func_DrawLine(Img, X0, Y0, X1, Y1, nG)
Img(X0, Y0) = nG;
Img(X1, Y1) = nG;
if abs(X1 - X0) <= abs(Y1 - Y0)
   if Y1 < Y0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1-Y0; dx = X1-X0;
      p = 2*dx; n = 2*dy - 2*dx; tn = dy;
      while (Y0 < Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 + 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dx; n = 2*dy + 2*dx; tn = dy;
      while (Y0 <= Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 - 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   end
else if X1 < X0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1 - Y0; dx = X1 - X0;
      p = 2*dy; n = 2*dx-2*dy; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 + 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dy; n = 2*dy + 2*dx; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 - 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   end
end
end
%--------------------------------------------------------------------------
 
% Rotar la imagen -8�
% g = -8;
% IR = imrotate(IM,g);
%--------------------------------------------------------------------------

%Mostrar el histograma de una imagen---------------------------------------
% histogram(I)
%--------------------------------------------------------------------------

%Fusionar dos imagenes-----------------------------------------------------
% C = imfuse(Im,Ifill,'blend','Scaling','joint');
%--------------------------------------------------------------------------


