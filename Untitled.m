% Load in image.
rgbImage = imread('d.png');
% Get the dimensions of the image.
[rows, columns, numberOfColorChannels] = size(rgbImage)
subplot(2, 2, 1);
imshow(rgbImage);
axis('on', 'image');
title('Original Image', 'FontSize', fontSize);
% Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
% Load Coordinates
s = load('annotation_0001.mat')
x = s.obj_contour(1, :);
y = s.obj_contour(2, :);
subplot(2, 2, 2);
plot(x, y, 'b*-', 'MarkerSize', 10, 'LineWidth', 2)
grid on;
title('Boundary Points', 'FontSize', fontSize);
xlabel('x', 'FontSize', fontSize);
ylabel('y', 'FontSize', fontSize);
% Show them over the image.
subplot(2, 2, 3);
imshow(rgbImage);
axis('on', 'image');
title('Original Image with Boundary', 'FontSize', fontSize);
hold on;
plot(x, y, 'b*-', 'MarkerSize', 9, 'LineWidth', 2)
% Create a mask from the coordinates.
mask = poly2mask(x, y, rows, columns);
subplot(2, 2, 4);
imshow(mask);
axis('on', 'image');
title('Boundary made into a Mask', 'FontSize', fontSize);
