Imagen = 'diente.jpg';
I = imread(Imagen);%obtener imagen
G = rgb2gray(I); %escala de grises

[f,c] = size(G);
O = G;

S = edge(G, 'Sobel');

for i = 1:f
    for j = 1:c
        x = G(i,j);
         if x <= 254 && x >= 235
             G(i,j) = 0;
         end
    end
end

subplot(1,3,1), imshow(O), title('Original');
subplot(1,3,2), imshow(S), title('Sobel');
subplot(1,3,3), imshow(G), title('Caries');
x = 0;
y = 0;
termino = true;
while termino
    termino = false;
end
% subplot(2,2,1), imshow(I), title('Original');
% subplot(2,2,2), imshow(IF), title('Filtro');
% subplot(2,2,3), imshow(EG), title('Gris');
% subplot(2,2,4), imshow(label2rgb(lblImg)), title('Borde');
