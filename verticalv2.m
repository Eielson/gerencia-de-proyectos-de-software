Im = imread('diente.jpg');
[rows, columns, numberOfColorChannels] = size(Im);
if numberOfColorChannels > 1
   I = rgb2gray(Im); 
else
    I = Im;
end
[height, width, dim] = size(I);
%Bottom-hat filtered
radio = round(width/4);
se = strel('disk',radio);
%B = imbothat(I,se);
J = adapthisteq(I,'clipLimit',0.02,'Distribution','rayleigh');
IM = imsubtract(imadd(J,imtophat(J,se)),imbothat(J,se)); %Imagen Mejorada
IM2 = I; %imsubtract(imadd(I,imtophat(I,se)),imbothat(I,se));

[f,co] = size(I);

%Para dibujar el grafico de las intensidades en el sentido horizontal------
ejeX = int32(zeros(1,f));
ejeY = linspace(1,f,f);
suma = 0;

for i = 1:f
    for j = 1:co
        valor = IM(i,j);       
        suma = int32(suma) + int32(valor);
    end
    ejeX(i) = suma;
    suma = 0;
end

[maxX, maxIndX] = max(ejeX(:));
[minX, minIndX] = min(ejeX(:));

if minIndX > maxIndX
    [maxX2, maxIndX2] = max(ejeX(minIndX:f));
    maxIndX2 = minIndX + maxIndX2 - 1;
    %IC = imcrop(IM,[1 maxIndX co maxIndX2]);
else
    [maxX2, maxIndX2] = max(ejeX(1:minIndX));
    %IC = imcrop(IM,[1 maxIndX2 co maxIndX]);
end

%plot(ejeX,ejeY);
%res = func_DrawLine(I, minIndX, 1, minIndX, co, 255);
%--------------------------------------------------------------------------

%Encontrar los puntos de corte horizontal----------------------------------
ancho = round(co*0.025);
altura = abs(maxIndX2 - maxIndX);
if maxIndX2 - maxIndX > 0
    posYminima = maxIndX;
else
    posYminima = maxIndX2;
end

LHX = [];
LHY = [];
gLineaHorizontal = I;
j= 0;
while j < co
     limiteX = j + ancho;
     if limiteX > co
         limiteX = co;
         ancho = co - j;
     end
     limiteY = posYminima + altura;
     posXminima = limiteX - ancho + 1;
     suma = 0;
     eX = [];
     eY = linspace(posYminima, limiteY, altura + 1);
     for j = posYminima:limiteY
        for k = posXminima:limiteX
            valor = I(j,k);       
            suma = int32(suma) + int32(valor);
        end
        eX = [eX suma];
        suma = 0;
     end
     %figure;plot(eX,eY);
     [valor, indice] = min(eX(:));
     if posXminima == 1
         LHX = [LHX posXminima];
     elseif limiteX == co
         LHX = [LHX limiteX];
     else
         LHX = [LHX posXminima + floor(ancho/2)];
     end
     LHY = [LHY eY(indice)];
     j = limiteX;
     %gLineaHorizontal = func_DrawLine(gLineaHorizontal, eY(indice), posXminima, eY(indice), limiteX, 255);
end

%Obtener punto por punto del corte horizontal
xx = 1:1:co;
yy = pchip(LHX,LHY,xx);
% p = plot(LHX,LHY,'o',xx,yy);
% h = findobj(p,'Type','line');
% x=get(h,'Xdata');
% y=get(h,'Ydata');

LH = zeros(co,1);
for i = 1: co
    v = floor(yy(i));
    LH(i) = v;
end
R = I;
resultado = I;
for i = 1:co
    resultado(LH(i),i)=255;
    R(LH(i),i)=255;
end

%--------------------------------------------------------------------------


%-----------------------------CORTE VERTICAL-------------------------------

XV = linspace(1,co,co); 
YV1 = []; %Mandibula superior
YV2 = []; %Mandibula inferior

suma1 = 0;
suma2 = 0;

%Mandibula superior
for j = 1:co
    for i = 1:f
        if i < LH(j)
            valor1 = IM2(i,j);
            suma1 = int32(suma1) + int32(valor1);
        elseif i > LH(j)
            valor2 = IM2(i,j);
            suma2 = int32(suma2) + int32(valor2);
        end
    end
    YV1 = [ YV1 suma1];
    YV2 = [ YV2 suma2];
    suma1 = 0;
    suma2 = 0;
end
%figure; plot(XV, YV1);
%figure; plot(XV, YV2);

%Aplicar promedio
PRO = YV1;
entrar = 1;
for j = 1:2
    [fil, col] = size(PRO);
    i = 3;
    while i < col - 2
        v1 = PRO(i - 2);
        v2 = PRO(i - 1);
        v3 = PRO(i);
        v4 = PRO(i + 1);
        v5 = PRO(i + 2);
        promedio = round((v1 + v2 + v3 + v4 + v5)/5);
        PRO(i) = promedio;
        i = i + 1;
    end
    if entrar == 1
        YV1 = PRO;
        entrar = 0;
    else
        YV2 = PRO;
    end
    PRO = YV2;
end
%figure;plot(XV,YV1);
%figure;plot(XV,YV2);

Y = double(YV1);
n = 4;
[pks,locs,~,prm] = findpeaks(-Y);
[~,i] = sort(prm,'descend');
menoresEjeY = Y(locs(i(1:n)));
menoresEjeX = locs(i(1:n));
UnionXY1 = [reshape(menoresEjeX,n,1) reshape(menoresEjeY,n,1)];
UnionXY1 = sortrows(UnionXY1,1);
%figure; plot(1:numel(Y),Y,'-',locs(i(1:n)),Y(locs(i(1:n))),'o ')

Y = double(YV2);
n = 4;
[pks,locs,~,prm] = findpeaks(-Y);
[~,i] = sort(prm,'descend');
menoresEjeY = Y(locs(i(1:n)));
menoresEjeX = locs(i(1:n));
UnionXY2 = [reshape(menoresEjeX,n,1) reshape(menoresEjeY,n,1)];
UnionXY2 = sortrows(UnionXY2,1);
%figure; plot(1:numel(Y),Y,'-',locs(i(1:n)),Y(locs(i(1:n))),'o ')
VLX1 = [];
VLY1 = [];
alturaVG = 20;
anchoVG = 5;
for pa = 1:n
    nX = UnionXY1(pa);
    limitePosY = LH(nX);
    limite = 1;
    puntoInicial = 1;
    VLPX = [];
    VLPY = [];
    while limite < limitePosY
        alturaV = alturaVG;
        anchoV = anchoVG;
        if nX + anchoV > co
            anchoV = co - nX - 1;
        elseif nX - anchoV < 1
            anchoV = nX - 1;
        end
        
        if limite + alturaV > limitePosY
            alturaV = limitePosY - limite;
        end 
        
        punto1 = [puntoInicial (nX - anchoV)];
        punto2 = [puntoInicial (nX + anchoV)];
        punto3 = [(puntoInicial + alturaV - 1) (nX - anchoV)];
        punto4 = [(puntoInicial + alturaV - 1) (nX + anchoV)];
        suma = 0;
        VP = []; %Vertical peque�o
        for j = punto1(2):punto2(2)
            for i = punto1(1):punto3(1)
                valor = IM2(i,j);
                suma = int32(suma) + int32(valor);
            end
            VP = [VP suma];
            suma = 0;
        end
        [valor, indice] = min(VP(:));
        pos = punto1(2) + indice - 1;
        nAnterior = nX;
        nX = pos;
        puntoInicial = puntoInicial + alturaV;
        if puntoInicial >= LH(nX)
            limite = limitePosY;
            nuevaAltura = LH(nAnterior) - punto31Anterior - 1;
            resultado = func_DrawLine(resultado, punto31Anterior, nAnterior, punto31Anterior + nuevaAltura, nAnterior, 255);
            VLPY = [VLPY (punto31Anterior + nuevaAltura)];
            VLPX = [VLPX nAnterior];
        else
            resultado = func_DrawLine(resultado, punto1(1), pos, punto3(1), pos, 255);
            limite = puntoInicial;
            limitePosY = LH(nX);
            
            if punto1(1) == 1
                VLPY = [VLPY 1];
            else
                VLPY = [VLPY (punto1(1) + floor((alturaV - 1)/2))];
            end
            VLPX = [VLPX nX];
        end
        punto31Anterior = punto3(1);
    end 
    VLY1 =[VLY1 VLPY -1];
    VLX1 = [ VLX1 VLPX -1];
end

VLX2 = [];
VLY2 = [];
for pa = 1:n
    nX = UnionXY2(pa);
    limitePosY = LH(nX);
    limite = f;
    puntoInicial = f;
    VLPX = [];
    VLPY = [];
    while limite > limitePosY
        alturaV = alturaVG;
        anchoV = anchoVG;
        if nX + anchoV > co
            anchoV = co - nX - 1;
        elseif nX - anchoV < 1
            anchoV = nX - 1;
        end
        if limite - alturaV < limitePosY
            alturaV = limite - limitePosY;
        end 
        
        punto3 = [puntoInicial (nX - anchoV)];
        punto4 = [puntoInicial (nX + anchoV)];
        punto1 = [(puntoInicial - alturaV + 1) (nX - anchoV)];
        punto2 = [(puntoInicial - alturaV + 1) (nX + anchoV)];
        suma = 0;
        VP = []; %Vertical peque�o
        for j = punto1(2):punto2(2)
            for i = punto1(1):punto3(1)
                valor = IM2(i,j);
                suma = int32(suma) + int32(valor);
            end
            VP = [VP suma];
            suma = 0;
        end
        [valor, indice] = min(VP(:));
        pos = punto1(2) + indice - 1;
        nAnterior = nX;
        nX = pos;
        puntoInicial = puntoInicial - alturaV;
        if puntoInicial <= LH(nX)
            limite = limitePosY;
            nuevaAltura = punto31Anterior - LH(nAnterior) - 1;
            resultado = func_DrawLine(resultado, punto31Anterior, nAnterior, punto31Anterior - nuevaAltura, nAnterior, 255);        
            VLPY = [VLPY (punto31Anterior - nuevaAltura)];
            VLPX = [VLPX nAnterior];
        else
            resultado = func_DrawLine(resultado, punto1(1), pos, punto3(1), pos, 255);
            limite = puntoInicial;
            limitePosY = LH(nX);
            if punto3(1) == f
                VLPY = [VLPY f];
            else
                VLPY = [VLPY (punto1(1) + floor((alturaV - 1)/2))];
            end
            VLPX = [VLPX nX];
        end
        punto31Anterior = punto3(1);
    end 
    VLY2 = [VLY2 VLPY -1];
    VLX2 = [VLX2 VLPX -1];
end

[~,columnas] = size(VLX1);
lineaVY = [];
lineaVX = [];
c = 1;
while c <= columnas
    if VLX1(c) ~= -1
        lineaVY = [lineaVY VLY1(c)];
        lineaVX = [lineaVX VLX1(c)];
    else
        [~, numCol] = size(lineaVX);
        if numCol > 1
            xxV1 = lineaVY(1):1:lineaVY(end);
            yyV1 = pchip(lineaVY,lineaVX,xxV1);
            %p = plot(lineaVY,lineaVX,'o',xxV1,yyV1);% 
            diferencia = lineaVY(end) - lineaVY(1) + 1;
            LVO = zeros(diferencia,1);
            eY = linspace(lineaVY(1),lineaVY(end),diferencia); 
            eX = zeros(diferencia,1);
            for d = 1: diferencia
                v = floor(yyV1(d));
                LVO(d) = v;
            end
            j = lineaVY(1);
            for d = 1:diferencia
                eX(d) = R(j,LVO(d));
                R(j,LVO(d)) = 255;
                j = j + 1;
            end             
            %figure;plot(eX,eY);
            %Y = double(eX);
            %n = 3;
            %[pks,locs,~,prm] = findpeaks(-Y);
            %[~,i] = sort(prm,'descend');
            %menoresEjeY = Y(locs(i(1:n)));
            %menoresEjeX = locs(i(1:n));
            %UnionXY1 = [reshape(menoresEjeX,n,1) reshape(menoresEjeY,n,1)];
            %UnionXY1 = sortrows(UnionXY1,1);
            %eX = reshape(eX,[1,diferencia]);
            medio = round((abs(eY(end) - eY(1))/2));
            ancho = 10;
            i = diferencia;
            comparar = 0;
            guardarV1 = 0;
            guardarI = 0;
            maxDif = 30;
            valido = 0;
            while i >= 1
                v1 = eX(i);
                if i - ancho < 0
                    v2 = eX(1);
                else
                    v2 = eX(i - ancho);
                end
                
                if comparar == 0
                    if v2 > v1
                        if v2 - v1 > 10
                            comparar = 1;
                            guardarV1 = v1;
                            guardarI = i; 
                            i = i - ancho;
                        else
                            i = i - 1; 
                        end
                    else
                       i = i - 1; 
                    end
                else
                    if v2 < v1
                        if abs(v2 - guardarV1) <= maxDif
                            if guardarI - i <= medio
                                valido = 1;
                                break;
                            end
                        end
                    end
                    i = i - ancho;
                end
            end
            
%             figure; findchangepts(eX,'MaxNumChanges',3);
%             [ipt,residual] = findchangepts(eX,'MaxNumChanges',3);
%             [iptf,iptc] = size(ipt);
%             
%             medio = round((abs(eY(end) - eY(1))/2)) - 10;
%             invalido = 0;
%             if iptf > 2
%                 if eX(1) < ipt(1) && ipt(2) - ipt(1) > medio && ipt(3) - ipt(2) < medio
%                     invalido = 1;
%                 elseif eY(1) < ipt(1) && ipt(3) - ipt(2) > medio && ipt(2) - ipt(1) < medio
%                     invalido = 1;
%                 end
%             else
%                 if ipt(2) - ipt(1) > medio
%                     invalido = 1;
%                 end
%             end
                if valido == 0
                    %Eliminar '-1'
                    VLX1(c) = [];
                    VLY1(c) = [];
                    c = c - 1;
                    for e = 1:numCol
                        VLX1(c) = [];
                        VLY1(c) = [];
                        c = c - 1;
                    end
                end
            %figure; plot(1:numel(Y),Y,'-',locs(i(1:n)),Y(locs(i(1:n))),'o ')
%             [vmin, indmin] = min(eX(:));
%             [vmax, indmax] = max(eX(:));
%             medio = floor((vmax - vmin)/2);
%             
%             pasoAbajo = 0;
%             pasoArriba = 0;
%             contador = 0;
%             intervalo = 10;
%             if eX(1) >= medio
%                 pasoArriba = 1;
%             else
%                 pasoAbajo = 1;
%             end
%             for k = 1:diferencia
%                 vx = eX(k);
%                 if vx <= medio && pasoAbajo == 0
%                     if medio - vx >= intervalo
%                         contador = contador + 1;
%                         pasoAbajo = 1;
%                         pasoArriba = 0;
%                     end
%                 elseif vx >= medio && pasoArriba == 0
%                     if vx - medio >= intervalo
%                         contador = contador + 1;
%                         pasoArriba = 1;
%                         pasoAbajo = 0;
%                     end
%                 end
%             end
%             
%             if contador < 2
%                 %Eliminar '-1'
%                 VLX1(i) = [];
%                 VLY1(i) = [];
%                 i = i - 1;
%                 for e = 1:numCol
%                     VLX1(i) = [];
%                     VLY1(i) = [];
%                     i = i - 1;
%                 end
%             end
        else
            %continue;
            R(lineaVY,lineaVX) = 0;
        end       
        lineaVY = [];
        lineaVX = [];
    end
    [~,columnas] = size(VLX1);
    c = c + 1;
end

[~,columnas] = size(VLX2);
lineaVY = [];
lineaVX = [];
i = 1;
while i <= columnas
    if VLX2(i) ~= -1
        lineaVY = [lineaVY VLY2(i)];
        lineaVX = [lineaVX VLX2(i)];
    else
        [~, numCol] = size(lineaVX); 
        if numCol > 1
            xxV1 = lineaVY(end):1:lineaVY(1);
            yyV1 = pchip(lineaVY,lineaVX,xxV1);
            %p = plot(lineaVY,lineaVX,'o',xxV1,yyV1);% 
            diferencia = lineaVY(1) - lineaVY(end) + 1;
            LVO = zeros(diferencia,1);
            eY = linspace(lineaVY(1),lineaVY(end),diferencia); 
            eX = zeros(diferencia,1);
            for d = 1: diferencia
                v = floor(yyV1(d));
                LVO(d) = v;
            end
            j = lineaVY(end);
            for d = 1:diferencia
                eX(d) = R(j,LVO(d));
                R(j,LVO(d)) = 255;
                j = j + 1;
            end 
            %figure;plot(eX,eY);
            [vmin, indmin] = min(eX(:));
            [vmax, indmax] = max(eX(:));
            medio = floor((vmax - vmin)/2);
            
            pasoAbajo = 0;
            pasoArriba = 0;
            contador = 0;
            intervalo = 10;
            if eX(1) >= medio
                pasoArriba = 1;
            else
                pasoAbajo = 1;
            end
            for k = 1:diferencia
                vx = eX(k);
                if vx <= medio && pasoAbajo == 0
                    if medio - vx >= intervalo
                        contador = contador + 1;
                        pasoAbajo = 1;
                        pasoArriba = 0;
                    end
                elseif vx >= medio && pasoArriba == 0
                    if vx - medio >= intervalo
                        contador = contador + 1;
                        pasoArriba = 1;
                        pasoAbajo = 0;
                    end
                end
            end
            
            if contador < 2
                %Eliminar '-1'
                VLX2(i) = [];
                VLY2(i) = [];
                i = i - 1;
                for e = 1:numCol
                    VLX2(i) = [];
                    VLY2(i) = [];
                    i = i - 1;
                end
            end
        else
            %continue;
            R(lineaVY,lineaVX) = 255;
        end
        lineaVY = [];
        lineaVX = [];
    end
    [~,columnas] = size(VLX2);
    i = i + 1;
end

[~,columnas] = size(VLX1);
for i = 1:columnas
    if VLX1(i) ~= -1
        R(VLY1(i), VLX1(i)) = 255;
    end
end
[~,columnas] = size(VLX2);
for i = 1:columnas
    if VLX2(i) ~= -1
        R(VLY2(i), VLX2(i)) = 0;
    end
end
imshow(R);
% p = plot(LHX,LHY,'o',xx,yy);
% h = findobj(p,'Type','line');
% x=get(h,'Xdata');
% y=get(h,'Ydata');
% 
% LH = zeros(co,1);
% for i = 1: co
%     v = floor(yy(i));
%     LH(i) = v;
% end
% R = I;
% resultado = I;
% for i = 1:co
%     resultado(LH(i),i)=255;
%     R(LH(i),i)=255;
% end

%[xi, yi, theProfile] = improfile(I,LHX,LHY);
%[fil, col] = size(xi);

% [fil, col] = size(LHX);
% %Convertir de gary a binario
% map = hsv(256);
% resultado = ind2rgb(I, map);
% resultado = I;
% for u = 1: col - 1
%     resultado = func_DrawLine(resultado, LHY(u), LHX(u), LHY(u + 1), LHX(u + 1), 0);
% end

%Mandibula superior
% for j = 1:co
%     for i = 1:f
%         comparar = resultado(i,j,1);
%         comparar2 =  resultado(i,j,3);
%         valor = IM2(i,j);
%         if comparar == 0 && comparar2 == 0
%             break;
%         else
%             suma = int32(suma) + int32(valor);
%         end
%     end
%     YV1 = [ YV1 suma];
%     suma = 0;
% end

%Mandibula inferior
% empezar = 0;
% for j = 1:co
%     for i = 1:f
%         if empezar == 0
%             comparar = resultado(i,j,1);
%             comparar2 =  resultado(i,j,3);
%             if comparar == 0 && comparar2 == 0
%                 empezar = 1;
%             end
%         else
%             valor = IM2(i,j);
%             suma = int32(suma) + int32(valor);
%         end
%     end
%     YV2 = [ YV2 suma];
%     suma = 0;
%     empezar = 0;
% end

% arregloTemporal = int32(zeros(1,abs(maxIndX - maxIndX2) + 1));
% puntosHorizontal = [];
% absoluto = abs(maxIndX2-maxIndX) + 1;
% for a = 1:co
%     if maxIndX > maxIndX2
%         contador = 1;
%         for b = maxIndX2:maxIndX
%             temporal = int32(I(b,a));
%             arregloTemporal(contador) = temporal;
%             contador = contador + 1;
%         end
%         [minimo, indice] = min(arregloTemporal(:));
%         arregloTemporal2 = [];
%         [filas, columnas] = size(arregloTemporal);
%         for p = 1:columnas
%             if arregloTemporal(p) == minimo
%                 arregloTemporal2 = [arregloTemporal2 (p + maxIndX2 - 1)];
%             end
%         end
%         promedio = mean(arregloTemporal2(:));
%         promedio = round(promedio);
%         puntosHorizontal = [puntosHorizontal promedio];
%     else
%         contador = 1;
%         for c = maxIndX:maxIndX2
%             temporal = int32(I(c,a));
%             arregloTemporal(contador) = temporal;
%             contador = contador + 1;
%         end
%         [minimo, indice] = min(arregloTemporal(:));
%         arregloTemporal2 = [];
%         [filas, columnas] = size(arregloTemporal);
%         for p = 1:columnas
%             if arregloTemporal(p) == minimo
%                 arregloTemporal2 = [arregloTemporal2 (p + maxIndX - 1)];
%             end
%         end
%         promedio = mean(arregloTemporal2(:));
%         promedio = round(promedio);
%         puntosHorizontal = [puntosHorizontal promedio];
%     end
% end
%--------------------------------------------------------------------------
% 
% %Dibujar el corte horizontal-----------------------------------------------
% [fil, col] = size(puntosHorizontal);
% resultado = Im;
% for u = 1: col - 1
%     resultado = func_DrawLine(resultado, puntosHorizontal(u), u, puntosHorizontal(u + 1), (u + 1), 255);
% end
% %--------------------------------------------------------------------------
% 
% %%%%%%%%%%%%%%%%%%PARA EL RECORTE VERTICAL%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IV = IM;
% for j = 1: co
%     for i = puntosHorizontal(j):f
%         IV(i,j) = 0;
%     end
% end
% 
% 
% %Para dibujar el grafico de las intensidades en el sentido vertical------
% ejeX = linspace(1,co,co);
% ejeY = int32(zeros(1,co));
% suma = 0;
% for j = 1:co
%     for i = 1:f
%         valor = IV(i,j);       
%         suma = int32(suma) + int32(valor);
%     end
%     ejeY(j) = suma;
%     suma = 0;
% end
% 
% %TF = islocalmin(ejeY);
% %plot(ejeX,ejeY,ejeX(TF),ejeY(TF),'r*')
% 
% level = graythresh(IV);
% IVB = imbinarize(IV,level);
% IVB = bwareaopen(IVB, 50);
% 
% ejeX = linspace(1,co,co);
% ejeY = int32(zeros(1,co));
% suma = 0;
% for j = 1:co
%     for i = 1:f
%         valor = IVB(i,j);       
%         suma = int32(suma) + int32(valor);
%     end
%     ejeY(j) = suma;
%     suma = 0;
% end
% 
% y = double(ejeY);
% n = 5;
% [pks,locs,~,prm] = findpeaks(-y);
% [~,i] = sort(prm,'descend');
% menoresEjeY = y(locs(i(1:n)));
% menoresEjeX = locs(i(1:n));
% UnionXY = [reshape(menoresEjeX,5,1) reshape(menoresEjeY,5,1)];
% UnionXY = sortrows(UnionXY,1);
% figure; plot(1:numel(y),y,'-',locs(i(1:n)),y(locs(i(1:n))),'o ')
% 
% P = vertcat(ejeX(1,1), UnionXY(:,1), ejeX(1, co));
% Q = vertcat(ejeY(1,1), UnionXY(:,2), ejeY(1, end));
% TF2 = islocalmax(Q);
% TF3 = islocalmin(Q);
% [tamP, ~] = size(P);
% minimosTF3 = [];
% for i = 1:tamP
%     if TF3(i) == 1
%         minimosTF3 = [minimosTF3 Q(i)];
%     end
% end
% 
% %figure; plot(P,Q,P(TF2),Q(TF2),'r*')
% 
% %Eliminar picos altos
% % TF2 = islocalmax(Q);
% % vof = 1;
% % while any(TF2 == 1) && vof == 1
% %     vof = 0;
% %     [tamP, ~] = size(P);
% %     i = 1;
% %     while i <= tamP
% %         pertenece = ismember(Q(i),minimosTF3);
% %         if TF2(i) == 1 && pertenece == 0
% %             TF2(i) = [];
% %             P(i) = [];
% %             Q(i) = [];
% %             tamP = tamP - 1;
% %             vof = 1;
% %         else
% %             i = i + 1;
% %         end
% %     end
% %       TF2 = islocalmax(Q);
% % end
% %figure; plot(P,Q)
% 
% %Analizar cada linea
% [tamP, ~] = size(P);
% inicioX = 1;
% ejeXporLinea = [];
% ejeYporLinea = [];
% lineasInvalidas = [];
% for i = 1:tamP
%     if i ~= 1 && i ~= tamP
%         if P(i) > P(i+1)
%             lineasInvalidas = [lineasInvalidas P(i)];
%         end
% %         ph = puntosHorizontal(P(i));
% %         ejeXporLinea = linspace(1, ph, ph);
% %         for ix = 1:ph
% %             %for iy = 1:puntosHorizontal(ix)
% %                 valor = IVB(ix,ph);       
% %           %      suma = int32(suma) + int32(valor);
% %            % end
% %             ejeYporLinea = [ejeYporLinea valor];
% %             %suma = 0;
% %         end
% %         figure;plot(ejeXporLinea,ejeYporLinea);
% % %         [maxValor, maxIndice] = max(ejeYporLinea(:));
% % %         if maxValor <= ejeYporLinea(1) || maxValor <= ejeYporLinea(end)
% % %             lineasInvalidas = [lineasInvalidas P(i)];
% % %         end
% % %         inicioX = P(i);
% %         ejeYporLinea = [];
%     end
% end
% 
% [tam, ~] = size(lineasInvalidas);
% i = 1;
% j = 1;
% while j <= tam
%     if P(i) == lineasInvalidas(j)
%         TF2(i) = [];
%         P(i) = [];
%         Q(i) = [];
%         j = j + 1;
%     else
%         i = i + 1;
%     end
% end
% 
% [tamP, ~] = size(P);
% resultado2 = IV;
% v = 1;
% for w = 1:co
%     if v <= tamP
%         if P(v) == w
%             if w ~= 1 && w ~= co
%                 resultado2 = func_DrawLine(resultado2, 1, w, f, w, 255);               
%             end
%             v = v + 1;
%         end
%     else
%         break;
%     end
% end
% 
% figure; imshow(resultado2);
% [tamP, ~] = size(P);
% inicioX = 1;
% ancho = round((co/(tamP - 1))/8);
% PuntosVerticales = [];
% for i = 1:tamP
%     if i ~= 1 && i ~= tamP
%         lineaVertical = [];
%         for iy = 1:f
%             menor = 255;
%             ix2 = 1;
%             if P(i) - ancho < 1
%                 ancho = P(i);
%             elseif P(i) + ancho > co
%                 ancho = co - P(i);
%             end
%             for ix = P(i) - ancho:P(i) + ancho
%                 valor = I(iy,ix);
%                 if valor < menor
%                     menor = valor;
%                     ix2 = ix;
%                 end
%             end
%             lineaVertical = [lineaVertical ix2];
%             %I(iy,ix2) = 255;
%         end
%         PuntosVerticales = vertcat(PuntosVerticales, lineaVertical);
%         lineaVertical = [];
%     end
% end
% 
% resultadoVertical = resultado;
% for i = 1:tamP - 2
%     for j = 1:f - 1
%         resultadoVertical = func_DrawLine(resultadoVertical, j, PuntosVerticales(i,j), (j + 1), PuntosVerticales(i, j+ 1), 255);
%     end
% end

%imshow(resultadoVertical);
%for i=180:-1:1

%Mostrar la imagen final
%y = double(ejeY);

%[peak_value, peak_location] = findpeaks(-y);
%plot(ejeX,y,ejeX(peak_location),peak_value,'r*')

% [pks,locs,~,prm] = findpeaks(-y);
% [~,i] = sort(prm,'descend');
% plot(1:numel(y),y,'-',locs(i(1:5)),y(locs(i(1:5))),'o ')

%imshow(resultado2)
%--------------------------------------------------------------------------

%Funcion para graficar lineas en una imagen--------------------------------
function Img = func_DrawLine(Img, X0, Y0, X1, Y1, nG)
Img(X0, Y0) = nG;
Img(X1, Y1) = nG;
if abs(X1 - X0) <= abs(Y1 - Y0)
   if Y1 < Y0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1-Y0; dx = X1-X0;
      p = 2*dx; n = 2*dy - 2*dx; tn = dy;
      while (Y0 < Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 + 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dx; n = 2*dy + 2*dx; tn = dy;
      while (Y0 <= Y1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; X0 = X0 - 1;
         end
         Y0 = Y0 + 1; Img(X0, Y0) = nG;
      end
   end
else if X1 < X0
      k = X1; X1 = X0; X0 = k;
      k = Y1; Y1 = Y0; Y0 = k;
   end
   if (X1 >= X0) & (Y1 >= Y0)
      dy = Y1 - Y0; dx = X1 - X0;
      p = 2*dy; n = 2*dx-2*dy; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 + 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   else
      dy = Y1 - Y0; dx = X1 - X0;
      p = -2*dy; n = 2*dy + 2*dx; tn = dx;
      while (X0 < X1)
         if tn >= 0
            tn = tn - p;
         else
            tn = tn + n; Y0 = Y0 - 1;
         end
         X0 = X0 + 1; Img(X0, Y0) = nG;
      end
   end
end
end
%--------------------------------------------------------------------------
