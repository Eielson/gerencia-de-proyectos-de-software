%Leyendo imagen
Im = imread('d2.png');
%A escala de grises
I = rgb2gray(Im);
%Aplicando Gauss
IGauss = imgaussfilt(I, 2);
%Haciendo superpixels
[L,N] = superpixels(IGauss,1000);

%---------------------------------------------------------------
% % Para dibujar bordes de color cyan de los superpixels
% BW = boundarymask(L); 
% imshow(imoverlay(IGauss,BW,'cyan'),'InitialMagnification',67)
%---------------------------------------------------------------

%Definir el color de cada pixel
pixelIdxList = label2idx(L);
ISuperpixel = zeros(size(IGauss),'like',I);
for superpixel = 1:N
     memberPixelIdx = pixelIdxList{superpixel};
     ISuperpixel(memberPixelIdx) = mean(IGauss(memberPixelIdx));
end

%Aplicando Fast Local Laplacian filtering
sigma = 0.4; 
alpha = 0.1;
IFLL = locallapfilt(ISuperpixel, sigma, alpha);

I = histeq(IFLL);
J = adapthisteq(I,'clipLimit',0.02,'Distribution','rayleigh'); 
%Aplicando el m�todo de Otsu
% IOtsu = Thresholding_Otsu(IFLL);
level = graythresh(J);
IOtsu = imbinarize(J,level);

IMediana = medfilt2(IOtsu);

%Mantener el objeto conectado m�s largo
CC = bwconncomp(IMediana);
numOfPixels = cellfun(@numel,CC.PixelIdxList);
[unused,indexOfMax] = max(numOfPixels);
biggest = zeros(size(IMediana));
biggest(CC.PixelIdxList{indexOfMax}) = 1;
imshow(biggest);

%Llenar los p�xeles negros dentro de los p�exeles blancos
IFilled = imfill(biggest);



imshowpair(I,IFilled,'montage');


