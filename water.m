Im = imread('diente.jpg');
I = rgb2gray(Im);

%Aplicando Gauss
IGauss = imgaussfilt(I, 2);
%Haciendo superpixels
[L,N] = superpixels(IGauss,1000);

%---------------------------------------------------------------
% % Para dibujar bordes de color cyan de los superpixels
% BW = boundarymask(L); 
% imshow(imoverlay(IGauss,BW,'cyan'),'InitialMagnification',67)
%---------------------------------------------------------------

%Definir el color de cada pixel
pixelIdxList = label2idx(L);
ISuperpixel = zeros(size(IGauss),'like',I);
for superpixel = 1:N
     memberPixelIdx = pixelIdxList{superpixel};
     ISuperpixel(memberPixelIdx) = mean(IGauss(memberPixelIdx));
end

%Aplicando Fast Local Laplacian filtering
sigma = 0.4; 
alpha = 0.1;
IFLL = locallapfilt(ISuperpixel, sigma, alpha);

%Aplicando el m�todo de Otsu
% IOtsu = Thresholding_Otsu(IFLL);
level = graythresh(IFLL);
IOtsu = imbinarize(IFLL,level);

IMediana = medfilt2(IOtsu);

% %Mantener el objeto conectado m�s largo
% CC = bwconncomp(IMediana);
% numOfPixels = cellfun(@numel,CC.PixelIdxList);
% [unused,indexOfMax] = max(numOfPixels);
% biggest = zeros(size(IMediana));
% biggest(CC.PixelIdxList{indexOfMax}) = 1;

%Rellenar
ILL = imfill(IMediana,'holes');

%ILL = imbinarize(ILL);


 imgDistTrans = bwdist(ILL); % Distance transform of original picture
 DistTransOfInv = bwdist(~ILL); % Distance transform of inverted picture
 NegDistTransOfInv = -DistTransOfInv; % Negated transform of inv image
 L = watershed(NegDistTransOfInv);
 finalImg = ILL;
 finalImg(L == 0) = 0;
 
 mask = imextendedmin(DistTransOfInv,2);
D2 = imimposemin(DistTransOfInv,mask);
Ld2 = watershed(D2);
bw3 = Im;
bw3(Ld2 == 0) = 0;
imshow(bw3);
 %imshowpair(Im,finalImg,'montage');