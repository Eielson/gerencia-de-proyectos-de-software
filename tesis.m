%Leyendo imagen
Im = imread('diente.jpg');
%A escala de grises
I = rgb2gray(Im);
%Aplicando Gauss
IGauss = imgaussfilt(I, 2);
%Haciendo superpixels
[L,N] = superpixels(IGauss,200);

%---------------------------------------------------------------
% % Para dibujar bordes de color cyan de los superpixels
% BW = boundarymask(L); 
% imshow(imoverlay(IGauss,BW,'cyan'),'InitialMagnification',67)
%---------------------------------------------------------------

%Definir el color de cada pixel
pixelIdxList = label2idx(L);
ISuperpixel = zeros(size(IGauss),'like',I);
for superpixel = 1:N
     memberPixelIdx = pixelIdxList{superpixel};
     ISuperpixel(memberPixelIdx) = mean(IGauss(memberPixelIdx));
end

%Aplicando Fast Local Laplacian filtering
sigma = 0.4; 
alpha = 0.1;
IFLL = locallapfilt(ISuperpixel, sigma, alpha);

%Aplicando el m�todo de Otsu
% IOtsu = Thresholding_Otsu(IFLL);
level = graythresh(IFLL);
IOtsu = imbinarize(IFLL,level);

IMediana = medfilt2(IOtsu);

%Mantener el objeto conectado m�s largo
CC = bwconncomp(IMediana);
numOfPixels = cellfun(@numel,CC.PixelIdxList);
[unused,indexOfMax] = max(numOfPixels);
biggest = zeros(size(IMediana));
biggest(CC.PixelIdxList{indexOfMax}) = 1;

%Rellenar
ILL = imfill(biggest,'holes');

% Watershed segmentation-----------------------------------------------------
IB = imbinarize(ILL);
imgDistTrans = bwdist(IB); % Distance transform of original picture
DistTransOfInv = bwdist(~IB); % Distance transform of inverted picture
NegDistTransOfInv = -DistTransOfInv; % Negated transform of inv image

% Compute label matrix
L = watershed(NegDistTransOfInv);

% Set catchments basins limits to zero effectively separating the ...objects
finalImg = IB;
finalImg(L == 0) = 0;
%----------------------------------------------------------------------------

%Gradientes direccionales
[IGx, IGy] = imgradientxy(finalImg,'prewitt');
%imshowpair(IGx,IGy,'montage');
 
spotsToRemove = IGx >= max(IGx(:))| IGx <= min(IGx(:));
spotsToFill = IGy >= max(IGy(:))| IGy <= min(IGy(:));
 
finalImg(spotsToFill == 1)= 1;
finalImg(spotsToRemove == 1)= 0;
 
 
%Cierre morfol�gico
SE = strel('line',2,90);
IJ = imclose(finalImg,SE);

%----------------------------------------------
%Objeto conectado m�s largo
CC2 = bwconncomp(IJ);
numOfPixels2 = cellfun(@numel,CC2.PixelIdxList);
[unused2,indexOfMax2] = max(numOfPixels2);
biggest2 = zeros(size(IJ));
biggest2(CC2.PixelIdxList{indexOfMax2}) = 1;
%----------------------------------------------
%Cierre morfol�gico
SE2 = strel('line',5,0);
IJ2 = imclose(biggest2,SE2);

%Rellenar
ILL2 = imfill(IJ2,'holes');

%Cierre morfol�gico
SE3 = strel('disk',10);
IJ3 = imclose(ILL2,SE3);
imshowpair(Im,IJ3,'blend');


