Im = imread('diente.jpg'); 
Ii = rgb2gray(Im);
I = histeq(Ii);
J = adapthisteq(I,'clipLimit',0.02,'Distribution','rayleigh'); 
%J = adapthisteq(I);

level = graythresh(J);
BW = imbinarize(J,level);
% imshowpair(I,BW,'montage')

level2 = graythresh(J);
BW2 = imbinarize(J,level);

se = strel('disk',1);
closeBW = imclose(BW2,se);

se2 = strel('disk',1);
afterOpening = imopen(closeBW,se2);

C = edge(afterOpening,'canny');
% J = adapthisteq(I,'clipLimit',0.02,'Distribution','rayleigh'); 
% imshowpair(I,J,'montage'); 
% title('Original Image (left) and Contrast Enhanced Image (right)')
subplot(2,2,1), imshow(I), title('Original');
subplot(2,2,2), imshow(J), title('Clahe');
subplot(2,2,3), imshow(BW), title('Otsu');
subplot(2,2,4), imshow(C), title('Clahe and Otsu');
